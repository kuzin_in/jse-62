package ru.kuzin.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.enumerated.EntitySort;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectListRequest extends AbstractUserRequest {

    @Nullable
    private EntitySort sortType;

    public ProjectListRequest(@Nullable EntitySort entitySortType) {
        this.sortType = entitySortType;
    }

    public ProjectListRequest(@Nullable String token) {
        super(token);
    }

}