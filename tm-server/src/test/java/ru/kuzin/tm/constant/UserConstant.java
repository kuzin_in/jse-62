package ru.kuzin.tm.constant;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.dto.model.UserDTO;
import ru.kuzin.tm.enumerated.Role;
import ru.kuzin.tm.model.User;

public interface UserConstant {

    int INIT_COUNT_USERS = 5;

    @Nullable
    UserDTO NULLABLE_USER = null;

    @Nullable
    User NULLABLE_USER_MODEL = null;

    @Nullable
    String NULLABLE_USER_ID = null;

    @Nullable
    String EMPTY_USER_ID = "";

    @Nullable
    String NULLABLE_EMAIL = null;

    @NotNull
    String EMPTY_EMAIL = "";

    @Nullable
    String NULLABLE_LOGIN = null;

    @NotNull
    String EMPTY_LOGIN = "";

    @Nullable
    String NULLABLE_PASSWORD = null;

    @NotNull
    String EMPTY_PASSWORD = "";

    @Nullable
    Role NULLABLE_ROLE = null;

}