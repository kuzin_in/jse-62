package ru.kuzin.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.enumerated.EntitySort;
import ru.kuzin.tm.enumerated.Status;
import ru.kuzin.tm.model.Task;

import java.util.List;

public interface ITaskService {

    void clear(@Nullable final String userId);

    @NotNull
    List<Task> findAll(@Nullable final String userId);

    boolean existsById(@Nullable final String userId, @Nullable final String id);

    @Nullable
    Task findOneById(@Nullable final String userId, @Nullable final String id);

    @Nullable
    Task findOneByIndex(@Nullable final String userId, @Nullable final Integer index);

    void removeById(@Nullable final String userId, @Nullable final String id);

    void removeByIndex(@Nullable final String userId, @Nullable final Integer index);

    @NotNull
    List<Task> findAll(@Nullable final String userId, @Nullable final EntitySort entitySort);

    void changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    void changeTaskStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    void add(@Nullable String userId, @Nullable Task task);

    void updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    void updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

}